import java.time.LocalDateTime;

public class CustomRuntimeException extends RuntimeException {

    private LocalDateTime dateTime;
    public  LocalDateTime getDateTime(){return dateTime;}

    public CustomRuntimeException(){}

    public CustomRuntimeException(String message){
        super(message);
    }

    public CustomRuntimeException(String message, LocalDateTime dateTime){

        super(message);
        this.dateTime=dateTime;
    }
}
