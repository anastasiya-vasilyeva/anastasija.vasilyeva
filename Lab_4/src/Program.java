import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void main(String[] args){

        LocalDateTime dateTime = LocalDateTime.now();
        List<DateTime> timeList = new ArrayList<>();

        for (int i = 1; i <= 30; i++) {
            DateTime someTime = new DateTime();
            someTime.setDateTime(dateTime);

            timeList.add(someTime);
            dateTime = dateTime.plusDays(1);
        }


        for (DateTime item : timeList) {
            try {
                defineDay(item);
            }
            catch (CustomRuntimeException ex) {
                System.out.println(ex.getMessage() + " " + ex.getDateTime());
            }
        }
    }


    public static void defineDay(DateTime item){

        LocalDateTime itemDateTime = item.getDateTime();
        int day = itemDateTime.getDayOfWeek().getValue();

        if (day != 6 && day != 7) {
            try {
                throw new CustomException("Это рабочий день", itemDateTime);
            } catch (CustomException ex) {
                System.out.println(ex.getMessage() + " " + ex.getDateTime());
            }
        } else {
            throw new CustomRuntimeException("Это выходной день", itemDateTime);
        }

    }
}
