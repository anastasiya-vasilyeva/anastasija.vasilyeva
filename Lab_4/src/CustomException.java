import java.time.LocalDateTime;

public class CustomException extends Exception {

    private LocalDateTime dateTime;
    public  LocalDateTime getDateTime(){return dateTime;}

    public CustomException(){}

    public CustomException(String message){
        super(message);
    }

    public CustomException(String message, LocalDateTime dateTime){

        super(message);
        this.dateTime=dateTime;
    }
}
