/**
 * Created by vasiljeva on 4/19/2016.
 */
public class Porshe extends Car {

    public void setFinishTime(int countOfLines, double distancePerLine){
        try {
            double totalTime = 0;
            double startSpeed = 0;

            for (int i = 0; i<=countOfLines; i++){
                double currentSpeed = Math.sqrt(Math.pow(startSpeed, 2) + 2*distancePerLine*acceleration);

                if(currentSpeed > maxSpeed/2){
                    handleability += (currentSpeed - maxSpeed/2)*0.005;
                }

                if(currentSpeed >= maxSpeed)
                    currentSpeed = maxSpeed;

                // calculate time for line
                double timeForLine = distancePerLine*2/currentSpeed;

                //calculate start speed for next line
                startSpeed = currentSpeed*handleability;

                // update total race time
                totalTime+=timeForLine;
            }
            finishTimeInSeconds = Math.round(totalTime);
        }
        catch (Exception ex){
            System.out.println("Something went wrong");
        }
    }
}

