/**
 * Created by vasiljeva on 4/19/2016.
 */
public abstract class Car {
    protected int carNumber ;

    public void setCarNumber(int carNumber) {
        this.carNumber = carNumber;
    }
    public int getCarNumber() { return this.carNumber; }

    protected double maxSpeed ;

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    protected double acceleration ;

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }
    public double getAcceleration() {
        return this.acceleration;
    }

    protected double handleability ;

    public void setHandleability(double handleability) {
        this.handleability = handleability;
    }
    public double getHandleability() {
        return this.handleability;
    }


    protected long finishTimeInSeconds;

    public abstract void setFinishTime(int countOfLines, double distancePerLine);
    public long getFinishTime() {return this.finishTimeInSeconds;}

}
