import org.omg.CORBA.SystemException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vasiljeva on 4/19/2016.
 */
public class Race {public static void main(String[] args){
    List<Car> carsArray = new ArrayList<Car>();

    int countOfLines = 20;
    double distancePerLine = 2;
    double distansPerlinesInMeters = distancePerLine*1000;

    // prepare cars list
    Porshe testPorshe = new Porshe();
    testPorshe.setCarNumber(1);
    testPorshe.setMaxSpeed(200);
    testPorshe.setAcceleration(15);
    testPorshe.setHandleability(0.5);
    testPorshe.setFinishTime(countOfLines, distansPerlinesInMeters);


    carsArray.add(testPorshe);

    Nissan testNissan = new Nissan();
    testNissan.setCarNumber(2);
    testNissan.setMaxSpeed(180);
    testNissan.setAcceleration(20);
    testNissan.setHandleability(0.4);
    testNissan.setFinishTime(countOfLines, distansPerlinesInMeters);

    carsArray.add(testNissan);

    Lada testLada = new Lada();
    testLada.setCarNumber(3);
    testLada.setMaxSpeed(190);
    testLada.setAcceleration(10);
    testLada.setHandleability(0.7);
    testLada.setFinishTime(countOfLines, distansPerlinesInMeters);

    carsArray.add(testLada);


    List<Car> sortedCars = getSortedVolumes(carsArray);

    for (Car item: sortedCars){

        long minutes = item.getFinishTime()/60;
        long seconds = Math.round(((double)item.getFinishTime()/60 - minutes) * 60);

        System.out.println( "Автомобиль под номером " +item.getCarNumber()+  " прошел трассу за " + minutes+ " минут и "+ seconds +" секунд");
    }
}

    public static List<Car> getSortedVolumes(List<Car> cars) throws SystemException {
        Collections.sort(cars, (car1, car2) -> {
            Car p1 = (Car) car1;
            Car p2 = (Car) car2;
            return Long.valueOf(car1.getFinishTime()).compareTo(car2.getFinishTime());
        });
        return cars;
    }

}
