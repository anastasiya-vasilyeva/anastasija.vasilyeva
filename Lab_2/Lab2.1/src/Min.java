/**
 * Created by vasiljeva on 4/12/2016.
 */
public class Min {
    public static void main(String[] args) {
        int[] mas = new int[]{1, 5, 6, 0};
        int min = mas[0];
        for (int i = 0; i < mas.length; i++) {
            if (min > mas[i])
                min = mas[i];
        }
        System.out.println(min);
    }
}
