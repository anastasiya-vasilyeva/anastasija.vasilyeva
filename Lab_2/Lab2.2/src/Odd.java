/**
 * Created by vasiljeva on 4/11/2016.
 */
public class Odd {
    public static void main(String[] args) {
        System.out.println("Все нечетные числа от 1 до 99:");
        for (int i = 1; i <= 99; i+= 2) {
            System.out.println(i);
        }
    }
}