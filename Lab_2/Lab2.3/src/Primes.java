import java.util.Scanner;

/**
 * Created by vasiljeva on 4/11/2016.
 */
public class Primes {
    public static void main(String[] args) {
        System.out.println("Введите n");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 2; i <= n; i++) {
            if (i%2 != 0 || i == 2)  {
                boolean isSimple = true;
                for (int j = 2; j < i; j++) {
                    if (i % j == 0) {
                        isSimple = false;
                        break;
                    }
                }
                if (isSimple) {
                    System.out.println(i);
                }
            }
        }
    }
}