import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vasiljeva on 4/12/2016.
 */
public class Amount {
    public static void main(String[] args) throws IOException {
        System.out.println("Введите число!");
        String inputValue = "";
        double sum = 0;
        while (!inputValue.toLowerCase().equals("сумма")) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            inputValue = br.readLine();
            if (isNumber(inputValue)) {
                sum += Double.parseDouble(inputValue);
            } else if (!inputValue.toLowerCase().equals("сумма")) {
                System.out.println("Введите число или слово 'Сумма'");
            }
            ;
        }
        long subResult = (long) sum;
        if (subResult == sum) {
            System.out.println("Сумма всех введенных чисел составляет: " + subResult);
        } else {
            System.out.println("Сумма всех введенных чисел составляет: " + sum);
        }
    }

    public static boolean isNumber(String value) {
        try {
            Double.parseDouble(value.replace(',', '.'));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
