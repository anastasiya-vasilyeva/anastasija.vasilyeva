/**
 * Created by vasiljeva on 4/7/2016.
 */
public class dbToInt {
    public static void main(String[] args) {
        int a = 100;
        double b = a;
        double c = 100.45;
        int d = (int) c;

        System.out.println("Int To Double  int - " + a + " double - " + b);

        System.out.println("Double To Int  double - " + c + " int - " + d);
    }
}
