import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by vasiljeva on 4/7/2016.
 */
public class calculator {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inputValue = br.readLine();
        String inputValue2 = br.readLine();
        String inputValue3 = br.readLine();

        double firstOperator = Double.parseDouble(inputValue);
        double secondOperator = Double.parseDouble(inputValue3);
        double result = 0;

        switch (inputValue2) {
            case "*":
                result = firstOperator * secondOperator;
                break;
            case "-":
                result = firstOperator - secondOperator;
                break;
            case "+":
                result = firstOperator + secondOperator;
                break;
            case "/":
                if (secondOperator == 0) {
                    System.out.println("На 0 делить нельзя");
                } else {
                    result = firstOperator / secondOperator;
                }
                break;
            default:
                System.out.println("Неизвестный оператор");
        }
        long longResult = (long) result;
        if (result == longResult) {
            System.out.println(longResult);
        } else {
            System.out.println(result);
        }
    }
}
