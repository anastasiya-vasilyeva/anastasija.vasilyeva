/**
 * Created by vasiljeva on 4/6/2016.
 */
public class SimpleTypes {


    public static void main(String[] args) {
        int x = 12;
        int y = 4;
        boolean c = true;
        boolean d = false;

        System.out.println("Арифметические операторы");
        System.out.println(x + "+" + y + "=" + (x + y));
        System.out.println(x + "/" + y + "=" + (x / y));
        System.out.println(x + "*" + y + "=" + (x * y));
        System.out.println(x + "-" + y + "=" + (x - y));
        System.out.println();

        System.out.println("Операторы сравнения");
        System.out.println(x + "<" + y + "=" + (x < y));
        System.out.println(x + ">" + y + "=" + (x > y));
        System.out.println(x + "<=" + y + "=" + (x <= y));
        System.out.println(x + ">=" + y + "=" + (x >= y));
        System.out.println(x + "==" + y + "=" + (x == y));
        System.out.println(x + "!=" + y + "=" + (x != y));
        System.out.println();

        System.out.println("Логические операторы");
        System.out.println(c + " | " + d + "=" + (c | d));
        System.out.println(c + " || " + d + "=" + (c || d));
        System.out.println(c + " & " + d + "=" + (c & d));
        System.out.println(c + " && " + d + "=" + (c && d));
        System.out.println("!" + c + " || " + d + "=" + (!c || d));
        System.out.println();

        System.out.println("Оператор присваивания");
        System.out.println("x= " + x + " " + "y= " + y + " " + "before  x = y");
        x = y;
        System.out.println("x= " + x + " " + "y= " + y + " " + "after  x = y");
    }
}