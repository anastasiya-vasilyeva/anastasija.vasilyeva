/**
 * Created by vasiljeva on 4/7/2016.
 */
public class primToStr {
    public static void main(String[] args) {
        byte by = 23;
        short sh = 254;
        int i = 8;
        long lon = 1235412364;
        char ch = 'c';
        float fl = 0.05f;
        double db = 0.0285;
        boolean bool = true;
        String byteToStr = Byte.toString(by);
        String shortToStr = Short.toString(sh);
        String intToStr = Integer.toString(i);
        String longToStr = Long.toString(lon);
        String charToStr = Character.toString(ch);
        String floatToStr = Float.toString(fl);
        String doubleToStr = Double.toString(db);
        String boolToStr = Boolean.toString(bool);

        System.out.println("To string");
        System.out.println(byteToStr);
        System.out.println(shortToStr);
        System.out.println(intToStr);
        System.out.println(longToStr);
        System.out.println(charToStr);
        System.out.println(floatToStr);
        System.out.println(doubleToStr);
        System.out.println(boolToStr);

        byte strToByte = Byte.parseByte(byteToStr);
        short strToShort = Short.parseShort(shortToStr);
        int strToInt = Integer.parseInt(intToStr);
        long strToLong = Long.parseLong(longToStr);
        char strToChar = charToStr.charAt(0);
        float strToFloat = Float.parseFloat(floatToStr);
        double strToDouble = Double.parseDouble(doubleToStr);
        boolean strToBool = Boolean.parseBoolean(boolToStr);
        System.out.println();

        System.out.println("From string");
        System.out.println(strToByte);
        System.out.println(strToShort);
        System.out.println(strToInt);
        System.out.println(strToLong);
        System.out.println(strToChar);
        System.out.println(strToFloat);
        System.out.println(strToDouble);
        System.out.println(strToBool);
    }
}

