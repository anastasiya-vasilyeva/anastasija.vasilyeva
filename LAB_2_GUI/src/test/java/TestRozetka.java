import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.w3c.dom.Element;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by User on 01.06.2016.
 */
public class TestRozetka {
    private WebDriver driver;
    private String baseUrl;

    @Before
    public void setUpBefore() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://rozetka.com.ua/";
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void checkLogo() throws InterruptedException {
               assertTrue("Логотип 'Розетка' не отображается", driver.findElement(By.xpath(".//*[@class=\"logo\"]")).isDisplayed());
                TimeUnit.SECONDS.sleep(5);
    }

    @Test
    public void checkAppleItem() throws InterruptedException {
             assertTrue("Раздел 'Apple' не отображается", driver.findElement(By.xpath(".//*[@id='m-main']//a[contains(text(), 'Apple')]")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }
    @Test
    public void checkMP3Item() throws InterruptedException {
             assertTrue("Раздел 'MP3' не отображается", driver.findElement(By.xpath(".//*[@class='m-main']//*[contains(text(),'MP3')]")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }
    @Test
    public void checkCityLinks() throws InterruptedException {
                      WebElement element = driver.findElement(By.xpath(".//*[@id='city-chooser']"));
        element.click();
        assertTrue("Город Одесса не отображается", driver.findElement(By.xpath("//div[@class='header-city-i']/a[contains(text(), 'Одесса')]")).isDisplayed());
        assertTrue("Город Киев не отображается", driver.findElement(By.xpath("//div[@class='header-city-i']/a[contains(text(), 'Киев')]")).isDisplayed());
        assertTrue("Город Харьков не отображается", driver.findElement(By.xpath("//div[@class='header-city-i']/a[contains(text(), 'Харьков')]")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }
    @Test
    public void checkBasketIsEmpty() throws InterruptedException {
               WebElement element = driver.findElement(By.xpath(".//*[@href='https://my.rozetka.com.ua/cart/']"));
        element.click();
        assertTrue("В корзине есть товар", driver.findElement(By.xpath(".//*[contains(text(), 'Корзина пуста')]")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }



}