import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.w3c.dom.Element;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by User on 01.06.2016.
 */
public class TestStackoverflow {
    private WebDriver driver;
    private String baseUrl;

    @Before
    public void setUpBefore() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://stackoverflow.com/";
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void checkFeaturedNumber() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(".//*[@class='bounty-indicator-tab']"));
        assertTrue("Kоличество в табке ‘featured’ меньше 300", Integer.parseInt(element.getText()) > 300);
    }

    @Test
    public void checkSocialButtons() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(".//*[@href='https://stackoverflow.com/users/signup?ssrc=head&returnurl=http%3a%2f%2fstackoverflow.com%2f']"));
        element.click();
        assertTrue("Кнопка Google не отображается", driver.findElement(By.xpath(".//*[@data-provider='google']/div[@class='text']")).isDisplayed());
        assertTrue("нопка Facebook не отображается", driver.findElement(By.xpath(".//*[@data-provider='facebook']/div[@class='text']")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);

    }

    @Test
    public void checkQuestionsDate() throws InterruptedException {
        List<WebElement> questions = driver.findElements(By.xpath(".//*[@id='question-mini-list']//h3/a"));
        int que = (int) (Math.random() * questions.size());
        questions.get(que).click();
        WebElement date = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[2]/p/b"));
        assertEquals("Вопрос был задан не сегодня", "today", date.getText());
    }
}
