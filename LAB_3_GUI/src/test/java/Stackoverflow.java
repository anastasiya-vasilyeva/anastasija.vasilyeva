import Stackoverflow.MainPage;
import Stackoverflow.SignUpButton;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import Stackoverflow.Questions;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by User on 14.06.2016.
 */
public class Stackoverflow {
    public WebDriver driver;
    public MainPage mainPage;

    @Before
    public void setUpBefore() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        mainPage = new MainPage(driver);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void checkFeaturedNumber() {
        assertTrue("Kоличество в табке ‘featured’ меньше 300", Integer.parseInt(mainPage.featuredBlock.getText()) > 300);
    }

    @Test
    public void checkSocialButtons() {
        SignUpButton singUp = mainPage.navigateToSingUpPage();
        assertNotNull("Google button is not found", singUp.googleButton);
        assertNotNull("Facebook button is not found", singUp.facebookButton);
    }

    @Test
    public void checkQuestionDate() {
        Questions questions = mainPage.navigateToQuestionPage();
        assertEquals("Вопрос был задан не сегодня", "today", questions.askedDate.getText());

    }
}