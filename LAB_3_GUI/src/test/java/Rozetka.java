import Lab3_Rozetka.Basket;
import Lab3_Rozetka.City;
import Lab3_Rozetka.HomePage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Created by User on 11.06.2016.
 */
public class Rozetka {
    private WebDriver driver;
    private HomePage homePage;


    @Before
    public void beforeTestDo() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void checkLogo() {
        Assert.assertTrue("Логотип не найден", homePage.image_logo.isDisplayed());
    }

    @Test
    public void checkAppleItem() {
        Assert.assertTrue("Apple не найден", homePage.appleItem.isDisplayed());
    }

    @Test
    public void checkMP3Item() {
        Assert.assertTrue("MP3 не найден", homePage.mP3Item.isDisplayed());
    }


    @Test
    public void checkCity() throws Exception {
        City city = homePage.navigateToCity();

        assertTrue("'Odessa' link can't be found", city.city_Odessa.isDisplayed());
        assertTrue("'Kharkiv' link can't be found", city.city_Kharkov.isDisplayed());
        assertTrue("'Kiev' link can't be found", city.city_Kiev.isDisplayed());
    }

    @Test
    public void basketIsEmpty() {
        Basket basket = homePage.navigateToBasket();
        assertNotNull("Basket is not empty", basket.txt_basketStatus);
    }

}