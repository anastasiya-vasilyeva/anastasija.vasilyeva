package Lab3_Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 13.06.2016.
 */
public class Basket {
            private WebDriver driver;

        public Basket(WebDriver driver) {
            PageFactory.initElements(driver, this);
            this.driver = driver;
        }

        @FindBy(xpath = "./*//*[contains(text(), 'Корзина пуста')]")
        public WebElement txt_basketStatus;


    }

