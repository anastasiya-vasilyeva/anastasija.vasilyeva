package Lab3_Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 13.06.2016.
 */
public class City {
    private WebDriver driver;

    public City(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Одесса')]")
    public WebElement city_Odessa;
    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Харьков')]")
    public WebElement city_Kharkov;
    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Киев')]")
    public WebElement city_Kiev;

}