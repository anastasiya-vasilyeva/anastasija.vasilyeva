package Lab3_Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 11.06.2016.
 */
public class HomePage {
    private WebDriver driver;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class=\"logo\"]")
    public WebElement image_logo;

    @FindBy(xpath = ".//*[@id='m-main']//a[contains(text(), 'Apple')]")
    public WebElement appleItem;

    @FindBy(xpath = "./*//*[@class='m-main']/*//*[contains(text(),'MP3')]")
    public WebElement mP3Item;

    @FindBy(xpath = "./*//*[@id='city-chooser']/a")
    public WebElement cityLink;

    @FindBy(xpath = "./*//*[@href='https://my.rozetka.com.ua/cart/']")
    public WebElement basketButton;

    public City navigateToCity() {
        cityLink.click();
        return new City(driver);

    }


    public Basket navigateToBasket(){
        basketButton.click();
        return new Basket(driver);
    }
}