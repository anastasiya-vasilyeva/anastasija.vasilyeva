package Stackoverflow;
import Stackoverflow.SignUpButton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by User on 14.06.2016.
 */
public class MainPage {
    private WebDriver driver;
    private String baseUrl;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='bounty-indicator-tab']")
    public WebElement featuredBlock;

    @FindBy(xpath = ".//*[@id='tell-me-more']")
    public WebElement signUpButton;

    @FindBy(xpath = ".//*[@id='question-mini-list']//div[2]/h3/a")
    public List<WebElement> questions;

    public int findElement() {
        int que = (int) (Math.random() * questions.size());
        return que;
    }

    public Questions navigateToQuestionPage() {
        questions.get(findElement()).click();
        return new Questions(driver);
    }

    public SignUpButton navigateToSingUpPage() {
        signUpButton.click();
        return new SignUpButton(driver);
    }
}