package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 14.06.2016.
 */
public class SignUpButton {
    private WebDriver driver;

    public SignUpButton(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@data-provider='google']/div[@class='text']")
    public WebElement googleButton;

    @FindBy(xpath = ".//*[@data-provider='facebook']/div[@class='text']")
    public WebElement facebookButton;


}