import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Program {

    public static void main(String[] args) throws IOException {
         String path;
         String folderName;
         int numOfCopies;

         System.out.println("Введите путь к файлу в формате c:\\Program Files\\...\\file Name.txt");

         //считаем путь к файлу из консоли, обернем в try catch
         try {
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
         path = br.readLine();

         File file = new File(path);
         // проверяем наличие файла по введенному адресу
         if (!file.exists() || !file.isFile()) {

         System.out.println("Файл не найден");
         }

         System.out.println("Введите имя папки для создания копии файла, например 'Origin', она должна содеожаться в пути к файлу");
         br = new BufferedReader(new InputStreamReader(System.in));
         folderName = br.readLine();

         // проверяем наличие папки в заданном пути
         if (!path.contains(folderName)) {

         System.out.println("Папка не найдена");
         }

         System.out.println("Введите количество копий, например 3, максимально 10");

         // считываем количество копий
         br = new BufferedReader(new InputStreamReader(System.in));
         numOfCopies = Integer.parseInt(br.readLine());

         // проверяем количество копий
         if (0 > numOfCopies || numOfCopies > 10) {
         System.out.println("Не верное количество");
         }

             List<File>  createdPaths =  createFolders(path, folderName, numOfCopies);

             for (File item : createdPaths) {
                 copyFileUsingStream(file, item);
             }
         }
         catch (Exception ex){
         System.out.println("Ошибка:" + ex.getMessage());
         }





    }

    static List<File> createFolders(String path, String folderName, Integer numOfCopies) throws IOException {

        Integer index = path.lastIndexOf(folderName) + folderName.length() + 1;
        String mainPath = path.substring(0, index);
        String tempPath = path.substring(index, path.length());
        Integer dirNumber = 0;


        String[] foldersArray = tempPath.split("\\\\");
        List<File> pathsArray = new ArrayList<>();

        for (Integer i = 0; i <= numOfCopies - 1; i++) {
            dirNumber++;
            String currentMainPath = mainPath;

            for (Integer j = 0; j <= foldersArray.length - 2; ) {

                File theDir = new File(currentMainPath + "\\" + foldersArray[j] + dirNumber);

                if (!theDir.exists()) {
                    currentMainPath = currentMainPath + "\\" + foldersArray[j] + dirNumber;
                    System.out.println("creating directory: " + theDir);
                    boolean result = false;

                    try {
                        theDir.mkdir();
                        result = true;
                    } catch (SecurityException se) {

                    }
                    if (result) {
                        System.out.println("DIR created");
                    }
                    j++;
                } else {
                    dirNumber++;
                }

                if (j == foldersArray.length - 1) {
                    File fileDirectory = new File(currentMainPath + "\\" + foldersArray[j]);
                    //Если требуемого файла не существует.
                    if (!fileDirectory.exists()) {
                        //Создаем его.
                        fileDirectory.createNewFile();
                    }
                    pathsArray.add(fileDirectory);
                }
            }
        }
        return pathsArray;
    }

    private static void copyFileUsingStream(File source, File dest) throws IOException {

        Thread x = new Thread() {
            @Override
            public void run() {
                InputStream is = null;
                OutputStream os = null;
                try {
                    is = new FileInputStream(source);
                    os = new FileOutputStream(dest);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                byte[] buffer = new byte[1024];
                int length;
                try {
                    while ((length = is.read(buffer)) > 0) {
                        os.write(buffer, 0, length);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    is.close();
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        x.start();


    }
}

